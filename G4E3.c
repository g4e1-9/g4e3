//GUIA MANEJO STRINGS EJERCICIO 3
//El usuario ingresa dos strings. 
//Mostrar en pantalla si son iguales o no, es decir, si tienen las mismas letras en las mismas posiciones.

#include <stdio.h>

int main()

{
    char A[20] = {0}; //Declaro las variables.
    char B[20] = {0};
    int flag = 0;
    
    printf("Ingrese la 1er palabra: "); //Solicito el ingreso de dos palabras para comparar.
    scanf("%s", A);
    printf("Ingrese la 2da palabra: ");
    scanf("%s", B);
    
    
    for (int i = 0; i<20; i++) //Comparo letra por letra del string A con el B. Si son diferentes la flag pasa de 0 a 1.
    {
        if (A[i] != B[i])
        {
            flag = 1;
        }
    }
    
    if (flag == 0) //Si flag vale 0, las palabras son iguales.
    {
        printf("Las palabras son iguales.");
    }
    
    else //Si flag vale 1, las palabras son distintas.
    
    {
        printf("Las palabras son distintas.");
    }
    
    return 0;
}